using System;
using System.Collections;
using HoodedCrow.Core;
using UnityEngine;
using UnityEngine.UI;

namespace HoodedCrow.Vista
{
    [RequireComponent(typeof(Canvas), typeof(GraphicRaycaster))]
    public class Vista : CachedMonoBehaviour
    {
        public string ID => _id;

        [SerializeField]
        private string _id = string.Empty;

        [SerializeField]
        private GameObject _root = null;
        
        [Space(10)]
        protected VistaManager _vistaManager = null;
        private WaitUntil _waitUntilDeactivated = null;

        protected override void Awake()
        {
            base.Awake();
            Debug.Log("Awake Done");
        }

        public void Init(VistaManager vistaManager)
        {
            _vistaManager = vistaManager;
            _waitUntilDeactivated = new WaitUntil(() => this.gameObject.activeSelf);
        }

        public void SetActive(bool value)
        {
            _root.SetActive(value);
        }
        
        public void Show()
        {
            StartCoroutine(ShowRoutine());
        }

        public void Hide()
        {
            StartCoroutine(HideRoutine());
        }

        private IEnumerator ShowRoutine()
        {
            yield return new WaitForEndOfFrame();
            yield return ShowStarted();
            _root.SetActive(true);
            yield return ShowEnded();
        }

        private IEnumerator HideRoutine()
        {
            yield return new WaitForEndOfFrame();
            yield return HideStarted();
            _vistaManager.HideVista(this);
            yield return _waitUntilDeactivated;
            yield return HideEnded();
        }

        protected virtual IEnumerator ShowStarted()
        {
            yield return null;
        }

        protected virtual IEnumerator ShowEnded()
        {
            yield return null;
        }

        protected virtual IEnumerator HideStarted()
        {
            yield return null;
        }

        protected virtual IEnumerator HideEnded()
        {
            yield return null;
        }
    }
}