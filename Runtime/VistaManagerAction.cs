namespace HoodedCrow.Vista
{
    public enum VistaManagerAction
    {
        Hide = 0,
        Show = 1,
        MarkedAsCurrent = 2,
        ShowAdditive = 3,
    }
}