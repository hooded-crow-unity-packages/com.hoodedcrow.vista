using System;
using UnityEngine;
using UnityEngine.UI;

namespace HoodedCrow.Vista.Examples
{
    public sealed class ExampleVista : Vista
    {
        [SerializeField]
        private Button _nextButton = null;
        [SerializeField]
        private string _nextVistaID = string.Empty;
        
        [Space(10)]
        [SerializeField]
        private Button _additionalButton = null;
        [SerializeField]
        private string _additionaltVistaID = string.Empty;

        [SerializeField]
        private bool _setAdditionalVistaToActive = true;
        
        protected override void Awake()
        {
            _nextButton.onClick.AddListener(NextButtonClicked);
            if (_additionalButton != null)
            {
                _additionalButton.onClick.AddListener(AdditiveVistaButtonClicked);
            }
        }

        private void NextButtonClicked()
        {
            this._vistaManager.ShowVista(_nextVistaID);
        }

        private void AdditiveVistaButtonClicked()
        {
            if(_setAdditionalVistaToActive)
            {
                _vistaManager.ShowVista(_additionaltVistaID, true);
            }
            else
            {
                _vistaManager.HideVista(_additionaltVistaID);
            }
        }
    }
}