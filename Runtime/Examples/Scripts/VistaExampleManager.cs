using UnityEngine;

namespace HoodedCrow.Vista.Examples
{
    public class VistaExampleManager :MonoBehaviour
    {
        [SerializeField]
        private VistaManager _vistaManager = null;
        private void Start()
        {
            _vistaManager.ShowVista("#1");
        }
    }
}