using System;
using System.Collections;
using System.Collections.Generic;
using HoodedCrow.Core;
using UnityEngine;

namespace HoodedCrow.Vista
{
    public class VistaManager : CachedMonoBehaviour
    {
        public event Action<Vista, VistaManagerAction> VistaChanged = delegate {  };

        [SerializeField]
        private Transform _vistaParent = null;
        private Vista _currentVista = null;
        
        [SerializeField]
        private List<Vista> _prefabsList = new List<Vista>();
        private Dictionary<string, Vista> _vistaCollection = new Dictionary<string, Vista>();
        private List<Vista> _additiveVistaList = new List<Vista>();

        public void ShowVista(Vista vista, bool additive = false)
        {
            ShowVista(vista.ID, additive);
        }
        public void ShowVista(string id, bool additive = false)
        {
            if (_vistaCollection.ContainsKey(id))
            {
                ShowVistaFromCollection(id, additive);
            }
            else
            {
                Vista vista = InstantiateVista(id);
                
                if(additive)
                {
                    _additiveVistaList.Add(vista);
                    
                    vista.Show();
                    VistaChanged.Invoke(_currentVista, VistaManagerAction.Show);
                }
                if (_currentVista == null)
                {
                    vista.Show();
                    VistaChanged.Invoke(_currentVista, VistaManagerAction.Show);
                    _currentVista = vista;
                }
                else if (additive == false)
                {
                    HandleCurrentVistaChange(vista);
                }
            }
        }

        public void HideCurrentVista(bool clearCurrentVista = false)
        {
            _currentVista.Hide();
            if (clearCurrentVista)
            {
                _currentVista = null;
            }
        }
        public void HideVista(Vista vista)
        {
            HideVista(vista.ID);
        }

        public void HideVista(string id)
        {
            Vista vista = _additiveVistaList.Find(v => v.ID.Equals(id));
            if (vista != null)
            {
                _additiveVistaList.Remove(vista);
            }

            if(_vistaCollection.ContainsKey(id))
            {
                vista = _vistaCollection[id];
                vista.SetActive(false);
                VistaChanged.Invoke(vista, VistaManagerAction.Hide);
            }
            else
            {
                Debug.Log($"Unknown vista ID {id}");
            }
        }

        public void HideAllAdditive()
        {
            StartCoroutine(HideAllAdditiveVistaRoutine());
        }

        private void ShowVistaFromCollection(string id, bool additive = false)
        {
            Vista vista = _vistaCollection[id];
            if (additive)
            {
                vista.Show();
                VistaChanged.Invoke(vista, VistaManagerAction.ShowAdditive);
                _additiveVistaList.Add(vista);
                return;
            }

            if (_currentVista != null)
            {
                HandleCurrentVistaChange(vista);
            }
            else
            {
                vista.Show();
                VistaChanged.Invoke(vista, VistaManagerAction.Show);
                SetCurrentVista(vista);
            }
        }

        private Vista InstantiateVista(string id)
        {
            Vista vistaPrefab = _prefabsList.Find(v => v.ID.Equals(id));
            if (vistaPrefab == null)
            {
                throw new ArgumentException($"Unknown vista id {id}");
            }
            
            Vista vista = Instantiate(vistaPrefab, _vistaParent, false);
            vista.Init(this);
            _vistaCollection[id] = vista;

            return vista;
        }

        private void SetCurrentVista(Vista vista)
        {
            _currentVista = vista;
            VistaChanged.Invoke(_currentVista, VistaManagerAction.MarkedAsCurrent);
        }

        private void HandleCurrentVistaChange(Vista vista)
        {
            HideCurrentVista();
            vista.Show();
            VistaChanged.Invoke(vista, VistaManagerAction.Show);
            _currentVista = vista;
            VistaChanged.Invoke(vista, VistaManagerAction.MarkedAsCurrent);
        }

        private IEnumerator HideAllAdditiveVistaRoutine()
        {
            while (_additiveVistaList.Count != 0)
            {
                Vista v = _additiveVistaList[0];
                v.Hide();
                yield return null;
            }
        }
    }
}