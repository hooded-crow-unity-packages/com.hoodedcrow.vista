# Hooded Crow - Vista System
 Vista system is prefab based system for GUI.
 
 ## Installation
 ### Package Manager
 Use the Unity Package Manager to add this package to your project.
 
 ### Hub
```TBA```
 
 ## License
 Created by Patryk 'huginn' Woźnicki [patryk.woznicki (at) huginn.me]
 As long as you retain this notice you can do whatever you want with this stuff. 
 If you like what I make, send me an email.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.